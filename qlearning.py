import matplotlib.pyplot as mpl # to plot the data
from scipy.signal import lfilter # this is used just to reduce the data noise while plotting with matplotlib
import timeit # measure how long the algorithm is taking to run
import copy # for deepcopies

from genetic_fitness import SPart
from lancichinetti_fitness import lancichinetti
from pizzuti_fitness import pizzuti
from normalizedcut_fitness import normalizedcut
from newman_fitness import newman

from util import to_dictionary, to_membership_list, NMI_to_dictionary, print_graphics, print_modularity

import igraph as ig # graph-related functions
import numpy as np # math functions

import time 


'''
	NOTE FOR THE STUDENT WHO PICKS UP THIS CODE:
		
		This is the main file. Here we have the implementation of Q-Learning for the
		community detection problem.

		This is a stateless problem, so The Q table is just a matrix 
		number_of_agents X number_of_actions
		(we do not have a state modeled into the Q-table, such as Q(s,a)
		for single agent qlearning)

		since each agent can select any other node as an action, the Q-table
		is a matrix NxN, for N vertices in the graph

		The rest of the theory is better explained in the article.

		In general this code is simple: we load the dataset into Igraph, create a 
		partitioning (a list of actions) that will be the input to the reward function
		(in this case, newman(g,p)). The result of the reward function is used to 
		update the Q-table of every agent. 
		In every episode, we choose a new partition, compute the reward and update 
		the Q-table. It is expected that the fitness value of each partition improves 
		over the episodes.

		
		to see the algorithm running using Zachary's Karate Club dataset,
		just type "python qlearning.py"

'''



class Qtable:

	# initiates the values of Q-table
	def __init__(self, n_node, n_action):
		
		self.table = np.zeros((n_node,n_action))





def cd_qlearning(k_number, alpha_value, n_episodes, seed, filename, decay, chosen_function=4):

	np.random.seed(seed)
	#initiates q table passing the number of vertices in g and number of clusters k
	Q = Qtable(g.vcount(),g.vcount())

	data = np.empty(n_episodes)
	data_nmi = np.empty(n_episodes)
	epsilon_value = np.empty(n_episodes)

	#dicitonary that maps every cluster to a list of nodes
	partitioning = []
	old_cluster = []
	old_counter = 0
	fitness = 0
	chosen_n = 0

	#assign every node to one community at random
	for n in g.vs.indices:

		if len(g.neighbors(n)) == 0:
			chosen_n = n
		else: 
			chosen_n = np.random.choice(g.neighbors(n))

		partitioning.append(chosen_n)



	#gets dictionary form of partition
	p = to_dictionary(partitioning)

	if chosen_function==0:
		base_Qvalue = lancichinetti(g,p)
	elif chosen_function==1:
		base_Qvalue = SPart(g,p)
	elif chosen_function==2:
		base_Qvalue = pizzuti(g,p)
	elif chosen_function==3:
		base_Qvalue = normalizedcut(g,p)
	else:
		base_Qvalue = newman(g,p)

	new_Qvalue = base_Qvalue

	l = to_membership_list(g,p)
	nmi = ig.compare_communities(TRUE_P,l,method='nmi')
	data[0] = new_Qvalue
	data_nmi[0] = nmi

	epsilon_value[0] = 1

	#print("partition "+str(partitioning))
	print("base: "+str(len(p.keys()))+" communities")
	print("initial fitness: "+str(base_Qvalue))

	eps = 1

	for ep in range(1,n_episodes):

		if ep%500 == 0:
			print("it "+str(ep))

		#eps = eps*decay
		#eps = 0.001**(ep/n_episodes)
		#eps = 0.001**(ep/4000)
		eps = decay**(ep/n_episodes)

		#all nodes do an action
		for n in g.vs.indices:

			nb = g.neighbors(n)

			highest = np.amax(Q.table[n])
			#np.where() return a tuple with (1-d answer, 2-d answer)
			nodes_with_highest_value = np.where(Q.table[n]==highest)
			#chooses one between the best
			n_prime = np.random.choice(nodes_with_highest_value[0])

			
			# for now this version is slower but it may be better in very large networks
			# in terms of performance it's about the same as above
			'''
			nb_value = Q.table[n][np.array(nb)]
			highest = np.amax(nb_value)
			nodes_with_highest_value = np.where(nb_value==highest)
			n_prime = nb[np.random.choice(nodes_with_highest_value[0])]
			'''


			random_n = np.random.random()

			#if (random_n > (1-ep/n_episodes)):
			#if (random_n > 0.08):
			if (random_n > eps):
				#puts n on the same community of his best friend
				chosen_n = n_prime
			else:
				if len(nb) == 0:
					chosen_n = n
				else: 
					#ng.append(n) APPENDING ITSELF TO THE LIST OF ACTIONS CREATES TOO MANY SINGLE CLUSTERS
					chosen_n = np.random.choice(nb)

			partitioning[n] = chosen_n

		p = to_dictionary(partitioning)

		
		if old_cluster != partitioning:
			if chosen_function==0:
				new_Qvalue = lancichinetti(g,p)
			elif chosen_function==1:
				new_Qvalue = SPart(g,p)
			elif chosen_function==2:
				new_Qvalue = pizzuti(g,p)
			elif chosen_function==3:
				new_Qvalue = normalizedcut(g,p)
			else:
				new_Qvalue = newman(g,p)
		
		old_cluster = copy.deepcopy(partitioning)
		

		fitness = new_Qvalue
		alpha = alpha_value

		for n in g.vs.indices:
			Q.table[n][partitioning[n]] = Q.table[n][partitioning[n]] + alpha*(fitness - Q.table[n][partitioning[n]])

		l = to_membership_list(g,p)
		nmi = ig.compare_communities(TRUE_P,l,method='nmi')
		data[ep] = new_Qvalue
		data_nmi[ep] = nmi
		epsilon_value[ep] = eps

		#if ep%1000==0:
		#		print("final "+str(p.keys()))
		#		print("Qvalue: "+str(new_Qvalue))

	print("seed: "+str(seed))
	print("final partition: "+str(len(p.keys()))+" communities")
	print("final fitness: "+str(new_Qvalue))
	print("final nmi = "+str(nmi))
	#print("final l = "+str(l))

	#print_graphics(data,data_nmi) # september

	return (nmi, new_Qvalue, base_Qvalue, data)







	

if __name__ == '__main__':

	#### DATASETS ####

	g = ig.Graph.Read_Lgl("datasets/zacharyLGL.txt")
	#g = ig.Graph.Read_GML("datasets/krebs.gml")
	#g = ig.Graph.Read_GML("datasets/dolphins.gml")
	#g = ig.Graph.Read_GML("datasets/football.gml")
	#g = ig.Graph.Read_Edgelist("datasets/email-Eu-coreEDGELIST.txt")
	#g = ig.Graph.Read_Edgelist("datasets/reactomeEDGELIST.txt")


	#### GROUND TRUTH PARTITIONS ####

	# karate club
	TRUE_P = [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

	#dolphin
	#TRUE_P = [1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1]

	# kreb's network of political books
	#TRUE_P =  [0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,0,0]

	# college football
	#TRUE_P = [7, 0, 2, 3, 7, 3, 2, 8, 8, 7, 3, 10, 6, 2, 6, 2, 7, 9, 6, 1, 9, 8, 8, 7, 10, 0, 6, 9, 11, 1, 1, 6, 2, 0, 6, 1, 5, 0, 6, 2, 3, 7, 5, 6, 4, 0, 11, 2, 4, 11, 10, 8, 3, 11, 6, 1, 9, 4, 11, 10, 2, 6, 9, 10, 2, 9, 4, 11, 8, 10, 9, 6, 3, 11, 3, 4, 9, 8, 8, 1, 5, 3, 5, 11, 3, 6, 4, 9, 11, 0, 5, 4, 4, 7, 1, 9, 9, 10, 3, 6, 2, 1, 3, 0, 7, 0, 2, 3, 8, 0, 4, 8, 4, 9, 11]

	#reactome ( doesn't have a known ground truth partition)
	#TRUE_P = [0]*6328

	'''
	#email-Eu-core
	f = open("datasets/email-Eu-coreGT.txt")
	allfile = f.readlines()
	f.close()
	TRUE_P = []
	for line in allfile:
		tup = [int(x.strip()) for x in line.split(" ")]
		TRUE_P.append(tup[1])
	assert(len(TRUE_P) == 1005)
	'''


	f = "filename.txt"

	g.to_undirected()


	N = 5
	average_nmi = np.empty(N)
	average_fitness = np.empty(N)
	average_initial_fitness = np.empty(N)
	for i in range(N):
		pair = cd_qlearning(2,0.15,4000,i+4,f,0.00001,chosen_function=4)
		average_nmi[i] = pair[0]
		average_fitness[i] = pair[1]
		average_initial_fitness[i] = pair[2]

	print(" results initial fitness : "+str(average_initial_fitness))
	print(" results nmi : "+str(average_nmi))
	print(" average nmi: " + str(np.mean(average_nmi)))
	print(" results fitness : "+str(average_fitness))
	print(" average fitness: " + str(np.mean(average_fitness)))


	'''
	#ORIGINAL PARTITION 
	l = NMI_to_dictionary(TRUE_P,12)
	r = newman(g,l)
	print("value of real partition: "+str(r))
	'''

	
	'''
	# PRINTING COMMUNITIES FOR ZACHARY
	layout = g.layout("kk")
	nl = []
	for idx in range(34):
		l = int(g.vs["name"][idx]) + 1
		nl.append(str(l))

	g.vs["label"] = nl

	print(g.vs["name"])
	print(g.vs["label"])

	color_dict = {0:"red", 1:"cyan", 2:"green", 3:"pink", 4:"white"}

	
	#nc = [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1]


	g.vs["color"] = [color_dict[i] for i in nc]
	#g.vs["color"] = ["red" for i in nc] ONE COMMUNITY
	print(nc)
	ig.plot(g, bbox = (600,600), margin = 20, label_dist=20, edge_arrow_size=0)
	'''
