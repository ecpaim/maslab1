import matplotlib.pyplot as mpl
from scipy.signal import lfilter
import numpy as np

import time

'''
	NOTE FOR THE STUDENT WHO PICKS UP THIS CODE:
		
		this file is just a bunch of helper functions.
		
		you should know that we can represent the partition of a graph in different ways:

		membership list: for a graph with 5 nodes and three communities (0,1 and 2), we would
		have, for example, [0,0,2,2,1].

		dictionary form: the same partition could be written as:
		{
			0: [0,1]
			1: [4]
			2: [2,3] "nodes 2 and 3 belong to community 2"
		}



		from the three functions to_membership_list, to_membership_listMANUAL and
		to_membership_listSTD, the last two are deprecated, so you can ignore 
		those and just use to_membership_list
'''




# transforms partition of vector form (as described in C. Chira 2012)
# to dictionary form where key = cluster id, value = list of index of nodes
def to_dictionary(partition):

	#start = time.process_time()

	clusters = {}
	k_number = 0

	nodes = set(list(range(len(partition))))

	#maps nodes to its cluster
	already_mapped = {}

	while len(nodes) != 0:
		i = nodes.pop()
		best_friend = partition[i]
		if best_friend in already_mapped.keys():
			clusters[already_mapped[best_friend]].append(i)
			already_mapped[i] = already_mapped[best_friend]
		else:
			new_cluster = []
			new_cluster.append(i)
			while best_friend not in new_cluster:
				if best_friend not in already_mapped.keys():
					nodes.remove(best_friend)
					new_cluster.append(best_friend)
					best_friend = partition[best_friend]
				else: break

			if best_friend in already_mapped.keys():
				k = already_mapped[best_friend]
				clusters[k].extend(new_cluster)
				for new_n in new_cluster:
					already_mapped[new_n] = k
			else:
				clusters[k_number] = new_cluster
				for new_n in new_cluster:
					already_mapped[new_n] = k_number
				k_number = k_number + 1

	#print("time " + str(time.process_time() - start))

	return clusters

def to_membership_list(graph,partition):
	m = [None]*graph.vcount()
	for k in partition:
		for v in partition[k]:
			m[v]=k
	return m

# SHOULD ONLY BE USED WHEN I MANUALLY TYPE A VECTOR OF A PARTITION INTO THE CODE
#transforms dictionary->"name" of each node to list required in igraphs' NMI method
def to_membership_listMANUAL(graph,partition):
	#doesn't work if the node is in more than one partition
	list = []
	for n in graph.vs["name"]:
		for k in partition:
			if int(n) in partition[k]:
				list.append(k)

	return list

def to_membership_listSTD(graph,partition):
	#doesn't work if the node is in more than one partition
	list = []
	for n in graph.vs.indices:
		for k in partition:
			if int(n) in partition[k]:
				list.append(k)

	return list

#transforms partition from membership_list to dictionary form
def NMI_to_dictionary(partition, k_number):
	dc = {}
	for k in range(k_number):
		dc[k] = []

	for i in range(len(partition)):
		dc[partition[i]].append(i)

	return dc #CAUTION: the output may be list of index OR list of igraph["name"]

def print_graphics(data, data_nmi):
	
	#first its episodes X SPart()
	f1 = mpl.figure(1)
	# applying lfilter to reduce image noise
	n=200 # the larger n is, the smoother the curve will be
	b = [1.0 /n]*n
	a = 1
	smooth_data = lfilter(b,a,data)

	#gets the indices of data to put in axis
	x_values = np.arange(len(data))
	mpl.plot(x_values,data)
	mpl.plot(x_values,smooth_data,color='c')
	mpl.xlabel("Episodes")
	mpl.ylabel("Fitness")
	#mpl.ylabel("SPart()",rotation=0)

	#second graphic is episodes X NMI
	f2 = mpl.figure(2)
	# applying lfilter to reduce image noise
	n=200 # the larger n is, the smoother the curve will be
	b = [1.0 /n]*n
	a = 1
	smooth_nmi = lfilter(b,a,data_nmi)
	#prints nmi graph
	mpl.plot(x_values,data_nmi,color='r')
	mpl.plot(x_values,smooth_nmi,color='m')
	mpl.xlabel("Episodes")
	mpl.ylabel("NMI")

	mpl.show()

def print_modularity(data):
	f1 = mpl.figure(1)

	colors = {0:'b', 1:'g', 2:'r', 3:'c', 4:'m', 5:'k'}

	x_values = np.arange(8000)
	n=200 # the larger n is, the smoother the curve will be
	b = [1.0 /n]*n
	a = 1
	
	x_values = np.arange(4000)
	smooth_data = lfilter(b,a,data[0])
	mpl.plot(x_values,smooth_data,color=colors[0], label = 'karate')

	x_values = np.arange(6000)
	smooth_data = lfilter(b,a,data[1])
	mpl.plot(x_values,smooth_data,color=colors[1], label = 'dolphin')

	x_values = np.arange(8000)
	smooth_data = lfilter(b,a,data[2])
	mpl.plot(x_values,smooth_data,color=colors[2], label = 'books')

	x_values = np.arange(8000)
	smooth_data = lfilter(b,a,data[3])
	mpl.plot(x_values,smooth_data,color=colors[3], label = 'football')

	x_values = np.arange(4000)
	smooth_data = lfilter(b,a,data[4])
	mpl.plot(x_values,smooth_data,color=colors[4], label = 'reactome')

	x_values = np.arange(4000)
	smooth_data = lfilter(b,a,data[5])
	mpl.plot(x_values,smooth_data,color=colors[5], label = 'emailEUcore')

	mpl.xlabel("Episodes")
	mpl.ylabel("Modularity")

	mpl.legend()
	mpl.show()
