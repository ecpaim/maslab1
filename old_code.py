
'''
	NOTE FOR THE STUDENT WHO PICKS UP THIS CODE:
		
	this whole file is just old versions of the algorithm, which either didn't work
	or were too slow

	you can ignore this file completely.

	just keep in mind that a community detection algorith must have 
	reasonable performance and time complexity


'''

# oldest version, in every episode all nodes are assigned to a new cluster, then the fitness is recalculed, then 
# every node is updated, but using the QNN representation to create partitions
def run_experimentQnn(k_number, alpha_value, n_episodes, seed, filename):
	np.random.seed(seed)


	#initiates q table passing the number of vertices in g and number of clusters k
	Q = Qtable(g.vcount(),g.vcount())

	#dicitonary that maps every cluster to a list of nodes
	partitioning = []

	chosen_n = 0

	data = np.empty(n_episodes)
	data_nmi = np.empty(n_episodes)

	for ep in range(n_episodes):
		if ep%1000 == 0:
			print("it "+str(ep))


        #initializes partitioning
		partitioning = []

		#assign every node to one community
		for n in g.vs.indices:

			#finds a list of nodes with the highest Q[n][n'] value
			#print("values para nodo "+str(n)+" sao "+str(Q.table[n]))
			highest_value = max(Q.table[n].items(), key = lambda x: x[1])
			nodes_with_highest_value = list()
			for node, value in Q.table[n].items():
					if value == highest_value[1]:
						nodes_with_highest_value.append(node)

			#chooses one between the best
			n_prime = np.random.choice(nodes_with_highest_value)


			random_n = np.random.random()

			if (random_n > (1-ep/n_episodes)):
			#if (random_n > (1/(ep+1))):
			#if (random_n > 0.7):
				#puts n on the same community of his best friend
				chosen_n = n_prime
			else:
				if len(g.neighbors(n)) == 0:
					chosen_n = n
				else: chosen_n = np.random.choice(g.neighbors(n))

			
			partitioning.append(chosen_n)

			#print("for node "+str(n)+" the maximum values are "+str(max_q_list))

		

		p = to_dictionary(partitioning)

		l = to_membership_list(g,p)
		nmi = ig.compare_communities(TRUE_P,l,method='nmi')

		new_Qvalue = newman(g,p)

		'''
		if ep%5000 == 0:
			print("ep "+str(ep)+" partitioning: "+str(partitioning))
			print("ep "+str(ep)+" fitness: "+str(new_Qvalue))
			print("ep "+str(ep)+" NMI: "+str(nmi))
		'''


		for n in g.vs.indices:

			#ADDING DECAYING LEARNING RATE TO ALPHA
			
			termo = (new_Qvalue - Q.table[n][partitioning[n]])**2

			#termo = 0.002
			#if ep%1000 == 0:
			#	print("termo: "+str(termo))
			
			#alpha = alpha_value*(1 - ep/n_episodes)*termo

			#alpha = 0.15*(1 - ep/n_episodes)*termo
			#alpha = 0.0065 
			alpha = 0.18
			
			#print("...updating value Q"+str(n)+" "+str(partitioning[n]))
			#updates table for (n, chosen_n)
			Q.table[n][partitioning[n]] = Q.table[n][partitioning[n]] + alpha*(new_Qvalue - Q.table[n][partitioning[n]])


		data[ep] = new_Qvalue
		data_nmi[ep] = nmi

	print("seed: "+str(seed))
	print("final partition: "+str(p))
	print("final fitness: "+str(new_Qvalue))
	l = to_membership_list(g,p)
	nmi = ig.compare_communities(TRUE_P,l,method='nmi')
	print("final nmi = "+str(nmi))

	'''
	f = open(filename,"a+")
	f.write(str(seed)+"\n")
	f.write(str(new_Qvalue)+"\n")
	f.write(str(nmi)+"\n")
	f.close()
	'''
		
	#print_graphics(data,data_nmi)


# the version which every episodes creates a new partition and change it N times per episode
# too slow
def run_isolated_actions(k_number, alpha_value, n_episodes, seed, filename):
	np.random.seed(seed)


	#initiates q table passing the number of vertices in g and number of clusters k
	Q = Qtable(g.vcount(),g.vcount())

	#dicitonary that maps every cluster to a list of nodes
	partitioning = []

	chosen_n = 0

	data = np.empty(n_episodes)
	data_nmi = np.empty(n_episodes)

	for ep in range(n_episodes):

		if ep%1000 == 0:
			print("it "+str(ep))


        #initializes partitioning
		partitioning = []

		#assign every node to one community at random
		for n in g.vs.indices:

			#finds a list of nodes with the highest Q[n][n'] value
			#print("values para nodo "+str(n)+" sao "+str(Q.table[n]))
			highest_value = max(Q.table[n].items(), key = lambda x: x[1])
			nodes_with_highest_value = list()
			for node, value in Q.table[n].items():
					if value == highest_value[1]:
						nodes_with_highest_value.append(node)

			#chooses one between the best
			n_prime = np.random.choice(nodes_with_highest_value)


			random_n = np.random.random()

			if (random_n > (1-ep/n_episodes)):
				#if (random_n > (1/(ep+1))):
				#if (random_n > 0.7):
					#puts n on the same community of his best friend
				chosen_n = n_prime
			else:
				if len(g.neighbors(n)) == 0:
					chosen_n = n
				else: chosen_n = np.random.choice(g.neighbors(n))

			partitioning.append(chosen_n)


	
		#gets dictionary form of partition
		p = to_dictionary(partitioning)

		base_Qvalue = newman(g,p)

		new_Qvalue = base_Qvalue

		if ep == 1000 or ep == n_episodes-1:
				print("base "+str(p))


		#changes position of every node and recalculates fitness
		nodes_random_order = np.random.permutation(g.vs.indices)
		if ep == 1000 or ep == n_episodes-1:
				print("base "+str(nodes_random_order))

		for n in nodes_random_order:

			#finds a list of nodes with the highest Q[n][n'] value
			#print("values para nodo "+str(n)+" sao "+str(Q.table[n]))
			highest_value = max(Q.table[n].items(), key = lambda x: x[1])
			nodes_with_highest_value = list()
			for node, value in Q.table[n].items():
					if value == highest_value[1]:
						nodes_with_highest_value.append(node)

			#chooses one between the best
			n_prime = np.random.choice(nodes_with_highest_value)


			random_n = np.random.random()

			if (random_n > (1-ep/n_episodes)):
				#if (random_n > (1/(ep+1))):
				#if (random_n > 0.7):
					#puts n on the same community of his best friend
				chosen_n = n_prime
			else:
				if len(g.neighbors(n)) == 0:
					chosen_n = n
				else: chosen_n = np.random.choice(g.neighbors(n))


			#removes node from its cluster and put it together with chosen_n
			for k in p:
				if n in p[k]:
					p[k].remove(n)
					#checks if cluster is empty, then remove it
					#CAUTION: THIS MAY BE INTRODUCING A BUG IF THE KEYS IN P ARE '2', '3', '4' FOR EXAMPLE
					old_cluster = k

					if not p[k]:
						del p[k] 
					break

			worked = False #debug
			for k in p:
				if chosen_n in p[k]:
					p[k].append(n)
					new_cluster = k
					worked = True
					break
	
			if(chosen_n==n):
				#checks smallest key that doesn't exist
				i = 0
				while True:
					if i not in p.keys():
						p[i]=[]
						p[i].append(chosen_n)
						if worked: 
							print('EERRRRRRRRROOOOO')
						break
					i+=1

			if old_cluster != new_cluster:
				new_Qvalue = newman(g,p)
			'''
			if new_Qvalue > base_Qvalue:
				fitness = 1
			else:
				fitness = -1
			'''
			#fitness =10*(new_Qvalue - base_Qvalue)
			fitness = new_Qvalue

			#base_Qvalue = new_Qvalue
			
			alpha = 0.1
			
			#print("...updating value Q"+str(n)+" "+str(partitioning[n]))
			#updates table for (n, chosen_n)
			Q.table[n][chosen_n] = Q.table[n][chosen_n] + alpha*(fitness - Q.table[n][chosen_n])


			if False:
				print("node "  +str(n) +" chosen_n "+str(chosen_n))
				print("new"+str(p))
				print("fitness = "+str(new_Qvalue)+" - "+str(base_Qvalue) +" = " +str(fitness))

		if ep == 1000 or ep == n_episodes-1:
				print("final "+str(p))
				print("Qvalue: "+str(new_Qvalue))


		l = to_membership_list(g,p)
		nmi = ig.compare_communities(TRUE_P,l,method='nmi')
		data[ep] = new_Qvalue
		data_nmi[ep] = nmi

	print("seed: "+str(seed))
	print("final partition: "+str(p))
	print("final fitness: "+str(new_Qvalue))
	print("final nmi = "+str(nmi))

	print_graphics(data,data_nmi)

# oldest version, in every episode all nodes are assigned to a new cluster, then the fitness is recalculed, then 
# every node is updated, CAN ONLY BE USED WITH FIXED K
def run_experiment(k_number, alpha_value, n_episodes, seed, filename):
	#print(g)
	np.random.seed(seed)

	#initiates q table passing the number of vertices in g and number of clusters k
	Q = Qtable(g.vcount(),k_number)

	#dicitonary that maps every cluster to a list of nodes
	partitioning = {}

	chosen_k = 0

	data = np.empty(n_episodes)
	data_nmi = np.empty(n_episodes)

	for ep in range(n_episodes):
		
        #initializes partitioning
		partitioning = {}
		for k in range(k_number):
			partitioning[k] =[]

		#assign every node to one community
		for n in g.vs.indices:

			#finds a list of clusters with the highest Q[n][k] value
			highest_value = max(Q.table[n].items(), key = lambda x: x[1])
			clusters_with_highest_value = list()
			for cluster, value in Q.table[n].items():
					if value == highest_value[1]:
						clusters_with_highest_value.append(cluster)
			#chooses one between the best
			k_prime = np.random.choice(clusters_with_highest_value)


			random_n = np.random.random()

			if (random_n > (1-ep/n_episodes)):
			#if (random_n > (1/(ep+1))):
			#if (random_n > 0.7):
				#puts n on the cluster with highest Q value
				chosen_k = k_prime
			else:
				#random cluster
				chosen_k = np.random.choice(list(partitioning.keys()))

			
			partitioning[chosen_k].append(n)

			#print("for node "+str(n)+" the maximum values are "+str(max_q_list))
		new_Qvalue = newman(g,partitioning)

		l = to_membership_list(g,partitioning)
		#print("partitioning:" + str(partitioning)+"\n\n")
		#print("membership list:" + str(l))
		nmi = ig.compare_communities(TRUE_P,l,method='nmi')

		'''
		if ep%5000 == 0:
			print("ep "+str(ep)+" partitioning: "+str(partitioning))
			print("ep "+str(ep)+" fitness: "+str(new_Qvalue))
			print("ep "+str(ep)+" NMI: "+str(nmi))
		'''


		for n in g.vs.indices:

			#gets cluster of node:
			for c in partitioning:
				if n in partitioning[c]:
					cluster = c

			#ADDING DECAYING LEARNING RATE TO ALPHA
			
			termo = (new_Qvalue - Q.table[n][cluster])**2

			#termo = 0.002
			#if ep%1000 == 0:
			#	print("termo: "+str(termo))
			
			#alpha = alpha_value*(1 - ep/n_episodes)*termo

			#alpha = 0.15*(1 - ep/n_episodes)*termo
			alpha = 0.0065 
			
			#if(ep%1000==0):
			#	print("alpha: "+str(alpha))

			#print("...updating value Q"+str(n)+" "+str(cluster))
			#updates table for (n_prime,partition)
			Q.table[n][cluster] = Q.table[n][cluster] + alpha*(new_Qvalue - Q.table[n][cluster])


		data[ep] = new_Qvalue
		data_nmi[ep] = nmi

	print("seed: "+str(seed))
	print("final partition: "+str(partitioning))
	print("final fitness: "+str(new_Qvalue))
	l = to_membership_list(g,partitioning)
	nmi = ig.compare_communities(TRUE_P,l,method='nmi')
	print("final nmi = "+str(nmi))

	'''
	f = open(filename,"a+")
	f.write(str(seed)+"\n")
	f.write(str(new_Qvalue)+"\n")
	f.write(str(nmi)+"\n")
	f.close()
	'''
	#print_graphics(data,data_nmi)
	

# the version which only one random partition is created at the first episode
def run_isolated_actions2(k_number, alpha_value, n_episodes, seed, filename, decay, chosen_function=3):

	np.random.seed(seed)


	#initiates q table passing the number of vertices in g and number of clusters k
	Q = Qtable(g.vcount(),g.vcount())

	#dicitonary that maps every cluster to a list of nodes
	partitioning = []

	chosen_n = 0

	data = np.empty(n_episodes)
	data_nmi = np.empty(n_episodes)

	epsilon_value = np.empty(n_episodes)


	#assign every node to one community at random
	for n in g.vs.indices:

		if len(g.neighbors(n)) == 0:
			chosen_n = n
		else: 
			chosen_n = np.random.choice(g.neighbors(n))

		partitioning.append(chosen_n)


	
	#gets dictionary form of partition
	p = to_dictionary(partitioning)

	if chosen_function==0:
		base_Qvalue = lancichinetti(g,p)
	elif chosen_function==1:
		base_Qvalue = SPart(g,p)
	elif chosen_function==2:
		base_Qvalue = pizzuti(g,p)
	else:
		base_Qvalue = newman(g,p)

	#base_Qvalue = newman(g,p)

	new_Qvalue = base_Qvalue

	l = to_membership_list(g,p)
	nmi = ig.compare_communities(TRUE_P,l,method='nmi')
	data[0] = new_Qvalue
	data_nmi[0] = nmi

	epsilon_value[0] = 1

	print("partition "+str(partitioning))
	print("base "+str(p))
	print("initial fitness: "+str(base_Qvalue))

	eps = 1

	for ep in range(1,n_episodes):

		if ep%500 == 0:
			print("it "+str(ep))

		#changes position of every node and recalculates fitness
		nodes_random_order = np.random.permutation(g.vs.indices)
		#if ep == 1000 or ep == n_episodes-1:DEBUG
		#		print("base "+str(nodes_random_order))

		eps = eps*decay
		#eps = 0.08
		for n in nodes_random_order:

			#finds a list of nodes with the highest Q[n][n'] value
			#print("values para nodo "+str(n)+" sao "+str(Q.table[n]))
			highest_value = max(Q.table[n].items(), key = lambda x: x[1])
			nodes_with_highest_value = list()
			for node, value in Q.table[n].items():
					if value == highest_value[1]:
						nodes_with_highest_value.append(node)

			#chooses one between the best
			n_prime = np.random.choice(nodes_with_highest_value)


			random_n = np.random.random()

			
			#if (random_n > (1-ep/n_episodes)):
			#if (random_n > 0.08):
			if (random_n > eps):
				#if (random_n > (1/(ep+1))):
				#if (random_n > 0.7):
					#puts n on the same community of his best friend
				chosen_n = n_prime
			else:
				if len(g.neighbors(n)) == 0:
					chosen_n = n
				else: 
					ng = g.neighbors(n)
					
					# THIS CREATES TOO MANY SINGLE CLUSTERS
					#ng.append(n)
					chosen_n = np.random.choice(ng)

			'''
			#removes node from its cluster and put it together with chosen_n
			for k in p:
				if n in p[k]:
					p[k].remove(n)
					#checks if cluster is empty, then remove it
					#CAUTION: THIS MAY BE INTRODUCING A BUG IF THE KEYS IN P ARE '2', '3', '4' FOR EXAMPLE
					old_cluster = k

					if not p[k]:
						del p[k] 
					break

			worked = False #debug
			for k in p:
				if chosen_n in p[k]:
					p[k].append(n)
					new_cluster = k
					worked = True
					break
	
			if(chosen_n==n):
				#checks smallest key that doesn't exist
				i = 0
				while True:
					if i not in p.keys():
						p[i]=[]
						p[i].append(chosen_n)
						if worked: 
							print('EERRRRRRRRROOOOO')
						break
					i+=1
			'''
			old_cluster = partitioning[n]
			new_cluster = chosen_n

			if old_cluster != new_cluster:
				partitioning[n] = chosen_n
				p = to_dictionary(partitioning)

				if chosen_function==0:
					new_Qvalue = lancichinetti(g,p)
				elif chosen_function==1:
					new_Qvalue = SPart(g,p)
				elif chosen_function==2:
					new_Qvalue = pizzuti(g,p)
				else:
					new_Qvalue = newman(g,p)
			'''
			if new_Qvalue > base_Qvalue:
				fitness = 1
			else:
				fitness = -1
			'''
			#fitness =10*(new_Qvalue - base_Qvalue)
			fitness = new_Qvalue

			#base_Qvalue = new_Qvalue
			
			alpha = alpha_value
			
			#print("...updating value Q"+str(n)+" "+str(partitioning[n]))
			#updates table for (n, chosen_n)
			Q.table[n][chosen_n] = Q.table[n][chosen_n] + alpha*(fitness - Q.table[n][chosen_n])


			if False:
				print("node "  +str(n) +" chosen_n "+str(chosen_n))
				print("new"+str(p))
				print("fitness = "+str(new_Qvalue))

		if ep%1000==0:
				print("final "+str(p))
				print("Qvalue: "+str(new_Qvalue))


		l = to_membership_list(g,p)
		nmi = ig.compare_communities(TRUE_P,l,method='nmi')
		data[ep] = new_Qvalue
		data_nmi[ep] = nmi
		epsilon_value[ep] = eps

	print("seed: "+str(seed))
	print("final partition: "+str(p))
	print("final fitness: "+str(new_Qvalue))
	print("final nmi = "+str(nmi))
	print("final l = "+str(l))


	#print_graphics(data,data_nmi)
	'''
	x_values = np.arange(len(epsilon_value))
	mpl.plot(x_values,epsilon_value)
	mpl.xlabel("Episodes")
	mpl.ylabel("Epsilon")
	mpl.show()
	'''
	return (nmi, new_Qvalue, base_Qvalue)


def newman3(graph,partition):

	total = 0.0

	for k in partition:

		cluster = graph.subgraph(partition[k])

		a = ai(graph,partition,k)

		# % of edges in module i - ai
		total += (len(cluster.get_edgelist())/len(graph.get_edgelist())) - a**2

		#print(' value %f' % ((len(cluster.get_edgelist())/len(graph.get_edgelist())) - a**2))

	return total

# % of edges with at least 1 end in module i
def ai(graph,partition,k):

	total = 0.0

	for x,y in graph.get_edgelist():
		if x in partition[k] or y in partition[k]:
			total = total + 1


	total = total/len(graph.get_edgelist())

	return total

#too slow
def newman2(graph,partition):
	nc = len(partition.keys())
	m = np.zeros(shape=(nc,nc))

	ne = len(graph.get_edgelist())


	# generating matrix e where each cell e(i,j) is the fraction of edges that connect 
	# the community i to community j
	for k in partition:
		cluster = graph.subgraph(partition[k])

		m[k][k] = len(cluster.get_edgelist())/ne
		
		for k2 in range(k+1,nc):
			e = 0
			for x,y in graph.get_edgelist():
				if x in partition[k] or y in partition[k]:
					if x in partition[k2] or y in partition[k2]:
						e = e + 1
			m[k][k2] = e/ne

			#WRONG! As said in article, each element must only be counted once
			#m[k2][k] = e/ne # matrix is simmetric

	total = 0.0

	#print(m)
	print(g.vs["name"])

	total = np.trace(m) - np.sum(m**2)

	return tota