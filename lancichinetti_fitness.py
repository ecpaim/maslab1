import igraph as ig
'''
	NOTE FOR THE STUDENT WHO PICKS UP THIS CODE:
		this function performed poorly on the community detection problem, but 
		maybe this code could be useful in the future
'''

#fitness function as described in "Detecting the overlapping 
# and hierarchicalcommunity structure in complex networks"
# (Lancichinetti, 2009)
def lancichinetti(graph,partition, alpha = 1):

	total = 0.0
	nc = len(partition.keys())

	for k in partition:
		cluster = graph.subgraph(partition[k])

		denominator = sum(graph.degree(partition[k]))**alpha
		if denominator != 0:
			total += sum(cluster.degree())/denominator

	total = total/nc
	#print('returning total for '+str(partition)+ 'equals to '+str(total))
	return total

if __name__ == '__main__':

	g = ig.Graph()
	g.add_vertices(8)
	g.add_edges([(0,1),(0,2),(0,3),(2,1),(3,1),(2,3),(5,6),(6,7)])

	partitioning = {0:[0,3,2,1],1:[4],2:[5,7,6]}

	r = lancichinetti(g,partitioning)

	print(" r value: %f" % r)
