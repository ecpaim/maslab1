import igraph as ig
from util import NMI_to_dictionary

'''
	NOTE FOR THE STUDENT WHO PICKS UP THIS CODE:
		this function performed poorly on the community detection problem, but 
		maybe this code could be useful in the future
'''

# fitness value of a node as proposed by pizzuti (2008)
def pizzuti(graph, partition,r = 2):

	nc = len(partition.keys())

	total = 0.0

	for k in range(nc):
		cluster = graph.subgraph(partition[k])

		# the score of each community is M(S)*vc
		# vc = volume of c = number of edges connecting vertices inside S
		# maybe we need to do 2*vc because the adjacency matrix is symmetric
		total += power_mean(cluster,partition[k],r)*2*len(cluster.get_edgelist())

		#print("power mean of community %d is %f" %(k, power_mean(graph,partition[k],r)))

	#normalize to -1 : 1
	total = total/len(graph.vs.indices)

	return total

def power_mean(cluster, S, r):

	size = len(S)

	S.sort() 

	total = 0.0

	for node in S:
		total += (cluster.degree(S.index(node))/size)**r

	total = total/size

	return total


# debugging
if __name__ == '__main__':
	'''
	g = ig.Graph()
	g.add_vertices(8)
	g.add_edges([(0,1),(0,2),(0,3),(2,1),(3,1),(2,3),(5,6),(6,7)])

	partitioning = {0:[0,3,2,1],1:[4],2:[5,7,6]}

	r = pizzuti(g,partitioning,2)

	print(" r value: %f" % r)
	'''


	g = ig.Graph.Read_Lgl("zacharyLGL.txt")

	partitioning = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
	#partitioning = [ 0, 0, 0, 0, 1, 1, 1, 0, 2, 2, 1, 0, 0, 0, 2, 2, 1, 0, 2, 0, 2, 0, 2, 3, 3, 3, 2, 3, 3, 2, 2, 3, 2, 2]
	in_order = []
	for node_name in g.vs["name"]:
		c = partitioning[int(node_name)]
		in_order.append(c)


	d = NMI_to_dictionary(in_order,2)

	print(d)
	r = pizzuti(g,d)
	'''
	out_of_order = []
	for node_name in g.vs["name"]:
		c = partitioning[int(node_name)]
		out_of_order.append(c)
	'''
	print(" r value: %f" % r)