# MASLAB1

Implementation of  a  multi-agent  reinforcement  learning algorithm  
that optimizes  Newman's  modularity to solve the community detection problem.

The algorithm is implemented in qlearning.py

Datasets avaliable are in the datasets folder

Python libraries required:
Igraph
Numpy
Matplotlib
timeit
scipy



