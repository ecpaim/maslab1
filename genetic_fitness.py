'''
	NOTE FOR THE STUDENT WHO PICKS UP THIS CODE:
		this function performed poorly on the community detection problem, but 
		maybe this code could be useful in the future
'''


# fitness evaluation of the node, as proposed by Camelia Chira
def SNode(graph,node, partition, k):

	cluster = graph.subgraph(partition[k])


	# partition[k].index(node) because the nodes in cluster and partition[k] are in the same order
	# 2*Kin - Ktotal is equivalent (mathematically) to Kin - Kout
	partition[k].sort()
	value = (2*cluster.degree(partition[k].index(node)) - graph.degree(node))/cluster.vcount()
	#this implementation gives no errors if some clusters have 0 nodes (because this function won't be called)
	return value

#fitness evaluation of a community k (C. Chira 2012)
def SComm(graph,partition,k):

	total = 0.0

	for node in partition[k]:
		sd_level = 0.0
		for nb in graph.neighbors(node):
			if nb in partition[k]:
				#print(" nb= "+str(nb))
				sd_level = sd_level + SNode(graph,nb,partition,k)
		#print("neighbors: "+str(graph.neighbors(node)))
		total = total + SNode(graph,node,partition,k) + 0.5*sd_level
		#print(" total NODE "+ str(node)+" total "+str(total))

	#print("PARTITION "+str(k)+ " VALUE "+ str(total))
	return total

#fitness evaluation of a partition(C. Chira 2012)
def SPart(graph,partition):
	nc = len(partition.keys())

	total = 0.0

	for k in range(nc):

		cluster = graph.subgraph(partition[k])

		vCi = len(cluster.get_edgelist())/len(graph.get_edgelist())

		#print(" VC1 IGUAL A ")

		#to avoid division by 0
		if cluster.vcount() > 0:
			total = total + SComm(graph,partition,k)*(vCi/cluster.vcount())
		#print(" total= "+str(total))

	total = total/nc
	#print("FINAL PARTITION VALUE: " +str(total))
	return total


### this is all just debug ####

#partitioning = {0:[1,2,3],1:[4,0],2:[5,6,7]}
#for node in partitioning[2]:
#	print(node)

#print("final partitioning: "+str(p))
'''
partitioning = {0:[1,2,3],1:[4,0],2:[5,6,7]}
cluster = g.subgraph(partitioning[2])
print(str(cluster.get_edgelist()))
print("Snode para nodo 1 "+str(SNode(g,1,partitioning,0)))
print("Snode para nodo 0 "+str(SNode(g,0,partitioning,1)))
#print("Snode para nodo 6 "+str(SNode(g,6,partitioning,2)))

partitioning = {0: [0, 6, 2, 3, 4, 5, 7, 1]}
print("Scomm para comm 0 "+str(SComm(g,partitioning,0)))
print("SPart para part "+str(SPart(g,partitioning)))

#print(str(SPart(g,partitioning)))
#cluster = g.subgraph(partitioning[0])
#print(str(cluster.get_edgelist()))

partitioning = {0:[0,1,2,3],1:[4],2:[5,6,7]}

print("Scomm para comm 0 "+str(SComm(g,partitioning,0)))
print("Scomm para comm 1 "+str(SComm(g,partitioning,1)))
print("Scomm para comm 2 "+str(SComm(g,partitioning,2)))
print(str(SPart(g,partitioning)))


cluster = g.subgraph(partitioning[0])
print(str(cluster.get_edgelist()))
'''