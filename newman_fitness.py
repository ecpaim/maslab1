import igraph as ig
import numpy as np
import timeit
from util import NMI_to_dictionary, to_membership_listSTD

'''
	NOTE FOR THE STUDENT WHO PICKS UP THIS CODE:
		this function (modularity) is the only fitness function that performed reasonably in the community detection problem
'''

# Newman's modularity for measuring quality of community structures,
# as described in "Finding community structure in networks using the eigenvectors of matrices" (2006)
# NOTE: The 2002 and 2004 Newman's articles do not describe correctly the implementation used by igraph and the academia.
# This 2006 article gives the correct formula, with Pij = - ki*kj/2m



# partition in dictionary -> list of index of nodes
def newman(graph,partition):
	l = to_membership_list(graph,partition)
	#print("L VALUE: "+str(l))
	return graph.modularity(l)


#MODULARITY DENSITY
# described in (Zhenping Li, Shihua Zhang, Rui-Sheng Wang, Xiang-Sun Zhang, and Luonan Chen. Quantitative function for community detection)
# for every community, [L(Ci,Ci) - L(Ci,CiHat)]/size(Ci)
# O(n*average_number_neighbors) currently
# DID NOT PERFORM WELL, better to use newman(graph,partition)
def newman_density(graph,partition):

	density = 0.0

	m_list = to_membership_list(graph,partition)
	
	
	#this is 3 times faster than using graph.to_undirected() and adj_matrix = graph.get_adjacency()
	for k in partition:
		cluster = graph.subgraph(partition[k])

		number_edges = cluster.ecount()
		number_nodes =  cluster.vcount()

		#compute outer degree
		degree = 0
			
		for v in partition[k]:
			for v2 in graph.neighbors(v):
				if m_list[v2] != k:
					degree +=1

		density += (2*number_edges - degree)/number_nodes
		#print("density = 2*"+str(number_edges)+" - "+str(degree)+"/"+str(number_nodes))
	

	#print(graph.get_edgelist())
	return density




# does exactly the same as to_membership_listSTD but faster
def to_membership_list(graph,partition):
	m = [None]*graph.vcount()
	for k in partition:
		for v in partition[k]:
			m[v]=k
	return m


# all code inside main is just for debugging
if __name__ == '__main__':
	
	g = ig.Graph()
	g.add_vertices(8)
	g.add_edges([(0,1),(0,2),(0,3),(2,1),(3,1),(2,3),(5,6),(6,7)])
	
	partitioning = {0:[0,1],1:[2,3],2:[4,5,7,6]}

	#g = ig.Graph.Read_Lgl("zacharyLGL.txt")
	#g = ig.Graph.Read_GML("dolphins.gml")
	#g = ig.Graph.Read_GML("krebs.gml")
	#g = ig.Graph.Read_GML("football.gml")
	#g = ig.Graph.Read_Edgelist("reactomeEDGELIST.txt")
	g = ig.Graph.Read_Edgelist("email-Eu-coreEDGELIST.txt")

	#email-Eu-core
	f = open("email-Eu-coreGT.txt")
	allfile = f.readlines()
	f.close()
	TRUE_P = []
	for line in allfile:
		tup = [int(x.strip()) for x in line.split(" ")]
		TRUE_P.append(tup[1])
	assert(len(TRUE_P) == 1005)


	r = g.modularity(TRUE_P)
	print("modularity of ground truth = "+str(r))
	print(g.summary())
	print(r)


