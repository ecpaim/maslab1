import igraph as ig
import numpy as np
import timeit
from util import NMI_to_dictionary, to_membership_list

'''
	NOTE FOR THE STUDENT WHO PICKS UP THIS CODE:
		this function performed poorly on the community detection problem, but 
		maybe this code could be useful in the future
'''


# normalized cut defined in "Distributed learning automata-based algorithm for community detection in complex networks" 
# (Mohammad Mehdi Daliri Khomami +)
# NC(partition) = (1/k)*sum(cut()/vol())
# where cut(c,!c) is the number of edges between community c and !c and
# vol(c) is the total degree of vertices that are members of communnity c
# multiplied by -1 because we want to maximize this function
def normalizedcut(graph,partition):

	normalized_cut = 0.0

	size_k = len(partition)

	m_list = to_membership_list(graph,partition)

	all_degrees = graph.degree()

	for k in partition:

		cluster = graph.subgraph(partition[k])

		#compute outer degree
		degree = 0
		total_degree = 0

		for v in partition[k]:
			total_degree += all_degrees[v]
			for v2 in graph.neighbors(v):
				if m_list[v2] != k:
					degree += 1

		if total_degree != 0:
			normalized_cut += degree / total_degree

	normalized_cut = - normalized_cut/size_k

	return normalized_cut



# debugging
if __name__ == '__main__':

	g = ig.Graph()
	g.add_vertices(8)
	g.add_edges([(0,1),(0,2),(0,3),(2,1),(3,1),(2,3),(5,6),(6,7)])
	
	partitioning = {0:[0,1],1:[2,3],2:[4,5,7,6]}

	

	g = ig.Graph.Read_Lgl("zacharyLGL.txt")

	# ZACHARY
	partitioning = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

	partitioning = [0]*34

	partitioning = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

	# ONLY NEEDED FOR ZACHARY
	
	in_order = []
	for node_name in g.vs["name"]:
		c = partitioning[int(node_name)]
		in_order.append(c)
	
	d = NMI_to_dictionary(in_order,2)

	#print(timeit.timeit("newman(g,d)","from __main__ import newman, g, d",number=10000))
	

	r = normalizedcut(g,d)

	print(r)